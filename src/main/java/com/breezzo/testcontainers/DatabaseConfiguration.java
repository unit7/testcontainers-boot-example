package com.breezzo.testcontainers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.support.incrementer.DataFieldMaxValueIncrementer;
import org.springframework.jdbc.support.incrementer.PostgreSQLSequenceMaxValueIncrementer;

import javax.sql.DataSource;

/**
 * @author breezzo
 * @since 12/2/17.
 */
@Configuration
public class DatabaseConfiguration {

    @Autowired
    private DataSource dataSource;

    @Bean
    public DataFieldMaxValueIncrementer dataFieldMaxValueIncrementer() {
        return new PostgreSQLSequenceMaxValueIncrementer(dataSource, "user_seq");
    }
}
