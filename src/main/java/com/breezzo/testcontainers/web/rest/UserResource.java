package com.breezzo.testcontainers.web.rest;

import com.breezzo.testcontainers.UserEntity;
import com.breezzo.testcontainers.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;

/**
 * @author breezzo
 * @since 12/2/17.
 */
@RequestMapping("/user")
@RestController
public class UserResource {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/{id}")
    public UserEntity getUser(@PathVariable("id") Long id) {
        return userRepository.getOne(id);
    }

    @PutMapping
    public ResponseEntity<UserEntity> createUser(@RequestBody UserEntity entity) {
        UserEntity savedEntity = userRepository.save(entity);
        return ResponseEntity.created(URI.create("/name/" + savedEntity.getId()))
                .body(savedEntity);
    }
}
