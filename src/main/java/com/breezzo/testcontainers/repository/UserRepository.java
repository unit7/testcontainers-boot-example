package com.breezzo.testcontainers.repository;

import com.breezzo.testcontainers.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.incrementer.DataFieldMaxValueIncrementer;
import org.springframework.stereotype.Repository;

/**
 * @author breezzo
 * @since 12/2/17.
 */
@Repository
public class UserRepository {

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    private DataFieldMaxValueIncrementer dataFieldMaxValueIncrementer;

    public UserEntity getOne(Long id) {
        return jdbcTemplate.queryForObject("SELECT * FROM \"user\" WHERE id = :id", new MapSqlParameterSource("id", id), (rs, row) -> {
            UserEntity result = new UserEntity();
            result.setId(rs.getLong("id"));
            result.setLogin(rs.getString("login"));
            return result;
        });
    }

    public UserEntity save(UserEntity entity) {
        long id = dataFieldMaxValueIncrementer.nextLongValue();
        jdbcTemplate.update("INSERT INTO \"user\"(id, login) VALUES(:id, :user)",
                new MapSqlParameterSource("user", entity.getLogin()).addValue("id", id));

        entity.setId(id);
        return entity;
    }
}
