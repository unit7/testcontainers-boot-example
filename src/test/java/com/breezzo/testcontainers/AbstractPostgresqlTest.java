package com.breezzo.testcontainers;

import org.junit.ClassRule;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.support.TestPropertySourceUtils;
import org.testcontainers.containers.PostgreSQLContainer;

/**
 * @author breezzo
 * @since 2/3/18.
 */
@ContextConfiguration(initializers = AbstractPostgresqlTest.PostgresqlInitializer.class)
public abstract class AbstractPostgresqlTest {
    private static final int PSQL_PORT = 5434;
    private static final String DATABASE = "test";
    private static final String USERNAME = "test";
    private static final String PASSWORD = "test";

    @ClassRule
    public static final PostgreSQLContainer PSQL = new PostgreSQLContainer<>("postgres:9.6")
            .withExposedPorts(PSQL_PORT)
            .withDatabaseName(DATABASE)
            .withUsername(USERNAME)
            .withPassword(PASSWORD);


    public static class PostgresqlInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        @Override
        public void initialize(ConfigurableApplicationContext applicationContext) {
            TestPropertySourceUtils.addInlinedPropertiesToEnvironment(applicationContext,
                    "spring.datasource.url=" + buildJdbcUrl(),
                    "spring.datasource.driver-class-name=org.testcontainers.jdbc.ContainerDatabaseDriver");
        }
    }

    private static String buildJdbcUrl() {
        return "jdbc:tc:postgresql://" + PSQL.getContainerIpAddress() + ":" + PSQL.getMappedPort(PSQL_PORT) + "/" + DATABASE + "?TC_DAEMON=true";
    }
}
