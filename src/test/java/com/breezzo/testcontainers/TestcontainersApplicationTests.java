package com.breezzo.testcontainers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestcontainersApplication.class)
@Transactional
public class TestcontainersApplicationTests extends AbstractPostgresqlTest {

	@Autowired
	private WebApplicationContext webApplicationContext;

	private MockMvc mockMvc;

	private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

	@Before
	public void setup() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	@Test
	public void createResource() throws Exception {
		MvcResult result = mockMvc.perform(
				MockMvcRequestBuilders.put("/user")
						.contentType(MediaType.APPLICATION_JSON_UTF8)
						.content("{ \"login\":\"it-test\"}"))
				.andExpect(MockMvcResultMatchers.status().is(HttpStatus.CREATED.value()))
				.andReturn();

		String stringResult = result.getResponse().getContentAsString();
		UserEntity savedEntity = OBJECT_MAPPER.readerFor(UserEntity.class).readValue(stringResult);

		Assert.assertNotNull(savedEntity);
		Assert.assertEquals("it-test", savedEntity.getLogin());

		mockMvc.perform(
				MockMvcRequestBuilders.get("/user/{id}", savedEntity.getId())
						.contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
				.andExpect(MockMvcResultMatchers.jsonPath("$.id").value(savedEntity.getId()))
				.andExpect(MockMvcResultMatchers.jsonPath("$.login").value(savedEntity.getLogin()));
	}
}
